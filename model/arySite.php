<?php
$arySite = array(
    'メルカリ' => array('url' => 'https://www.mercari.com/jp/search/?keyword=',
                       'favicon' => 'http://www.google.com/s2/favicons?domain=www.mercari.com'),
    'Amazon' => array('url' => 'https://www.amazon.co.jp/s/ref=nb_sb_noss?__mk_ja_JP=カタカナ&url=search-alias%3Daps&field-keywords=',
                       'favicon' => 'http://www.google.com/s2/favicons?domain=www.amazon.co.jp'),
    'ヤフオク' => array('url' => 'https://auctions.yahoo.co.jp/search/search?ei=utf-8&p=',
                       'favicon' => 'http://www.google.com/s2/favicons?domain=auctions.yahoo.co.jp'),
    'ヨドバシ' => array('url' => 'http://www.yodobashi.com/ec/category/index.html?word=',
                       'favicon' => 'http://www.google.com/s2/favicons?domain=www.yodobashi.com'),
    'ZOZOTOWN' => array('url' => 'http://zozo.jp/search/?p_keyv=',
                       'favicon' => 'http://www.google.com/s2/favicons?domain=zozo.jp'),
    );
?>