<!DOCTYPE html>
<html lang="ja">
<head>
    <meta charset="utf-8">
    <meta name="description" content="一括検索サービス">
    <meta name="keywords" content="CROSSER,crosser,商品,検索,横断検索,最安値,価格,値段,一括,Webサービス, 比較, 価格, amazon, ヨドバシ,
                                    ヤフオク,メルカリ, zozo">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Crosser</title>
    <!-- reset -->
    <link rel="stylesheet" href="css/reset.css">
    <!-- intro.js -->
    <link rel="stylesheet" href="css/introjs.css">
    <!-- common -->
    <link rel="stylesheet" href="css/common.css">
    <!-- index -->
    <link rel="stylesheet" href="css/index.css">
    <!-- loading -->
    <link rel="stylesheet" href="css/loading.css">
    <!-- font -->
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">
    <!-- jQuery UI -->
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/themes/smoothness/jquery-ui.css">
    <!-- analytics-->
    <?php include_once("../model/analyticstracking.php") ?>
</head>
<body ontouchend>
<div id="wrap">
    <div id="loading" class="is-hide">
        <div class="loading_icon"></div>
    </div>
    <!--  header -->
    <header>
        <nav id="main-nav">
            <ul class="clearfix">
                <li class="is-rel"><a href="index.php"
                       id="contact">
                        Contact</a>
                    <div id="contact-tooltip" class="is-hide">
                        <p>ご要望・バグ報告はこちらまで<br><span>crosser.inq@gmail.com</span></p>
                    </div>
                </li>
                <li><a href="" id="how-to">How To</a></li>
                <li><a href="/">Home</a></li>
                <span id="slide-line"></span>
            </ul>

        </nav>
    </header>

    <!-- content -->
    <div id="cont">
        <h1 id="site-title" 
           data-intro="はじめまして！<br>
                       Crosserは一回のキーワード入力で複数サイトで同時に検索ができるサービスです" 
           data-step="1">
           CROSSER
        </h1>
        <form action="" method="get" name="search-frm">
            <div id="search-wrap" data-intro="一括検索したいワードを入力して検索" data-step="2">
                <input type="text" name="search-box" id="search-box" placeholder="">
                <div id="btn-box">
                    <div id="search-btn">&nbsp;</div>
                </div>
            </div>
        </form>
        <div id="result-area" style="display: none;" class="" data-intro="検索すると結果が表示されます" data-step="3">
            <div id="result-top">
               <div id="search-word">
                  <p>keyword<span id="keyword">カバン</span></p>
               </div>
                <ul id="res-links">

                    <ul id="res-links" data-intro="１つ１つ確認もできますが、" data-step="4">
                        <li>
                            <a href="#">
                                <img src="http://www.google.com/s2/favicons?domain=www.mercari.com" alt="fav">
                                メルカリ
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="http://www.google.com/s2/favicons?domain=www.amazon.co.jp" alt="fav">
                                Amazon
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="http://www.google.com/s2/favicons?domain=auctions.yahoo.co.jp" alt="fav">
                                ヤフオク
                            </a>
                        </li>
                        <li>
                            <a href="#">
                                <img src="http://www.google.com/s2/favicons?domain=www.yodobashi.com" alt="fav">
                                ヨドバシ
                            </a>
                        </li>
                    </ul>
                </ul>
            </div>
            <div id="open-all-btn"
                 data-intro="ここから全部のサイトをタブで開けます<br>何か検索してみましょう<br>
                            <span class='tut-cap'>※ポップアップを許可してください</span>"
                 data-step="5">
                Open All Pages
            </div>
        </div>
    </div>
    <!--  footer  -->
    <footer>
        Copyright &copy; 2017 yir All Rights Reserved.
    </footer>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js"></script>
<script type="text/javascript" src="js/intro.js"></script>
<script type="text/javascript" src="js/index.js"></script>
<script type="text/javascript" src="js/submit.js"></script>
</body>
</html>