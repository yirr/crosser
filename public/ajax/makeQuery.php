<?php
//セキュリティ対策
header("Content-Type: application/json; charset=UTF-8");
header("X-Content-Type-Options: nosniff");

//common呼び出し
include '../../model/common.php';
//サイト情報(DBに移す？)
include '../../model/arySite.php';


if (!isAjax()) {
    die(json_encode(array('stat' => "参照不成功：不正な呼び出し")));
}

$errors = false;
$word = '';

//検索ワードある
if (!empty($_POST['word'])) {
    $word = $_POST['word'];
    //URLパラメータ用の検索ワードをSJISに変換
    $wordSjis = mb_convert_encoding($word , 'SJIS');

    //キーワードをURLにくっつける
    foreach ($arySite as $key => &$site) {
        if($key == 'ZOZOTOWN'){
            //zozoはSJIS
            $site['url'] .= urlencode($wordSjis);
        }else{
            $site['url'] .= urlencode($word);
        }
    }
    //URL情報とサニタイズした検索ワードを返す
    $res = array('query' => $arySite, 'word' => h($word));
    jsonEnc($res);
}else{
    $res['err'] = '未入力';
    jsonEnc($res);
}
function jsonEnc($param){
    echo json_encode($param, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP);
}

?>