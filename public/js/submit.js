$('form[name=search-frm]').submit(function () {
    //オートコンプリート閉じる
    $('#search-box').autocomplete('close').blur();

    //処理中はsubmitを無効に
    $(this).attr('onsubmit', 'return false;');

    let data = {
        word: $('input[name=search-box]').val()
    };

    //ajax
    if (data.word !== "") {
        sender(data, $(this));
    } else {
        console.log('jsCheck:Nothing Input.')
    }
    //submitによるページ遷移をさせない
    return false;
});

function sender(data, frmObj) {
    let def = $.ajax({
        type: "POST",
        url: "/ajax/makeQuery.php",
        data: data,
        dataType: "json",
        cache: false,
        timeout: 10000,
        //送信前
        beforeSend: function () {
            //loading表示
            dispLoading();
        }
    });
    setTimeout(function () {
        $.when(def).done(function (res) {
            // console.log('responseData:');
            // console.log(res);
            if ('err' in res) {
                //エラー
                console.log('ERROR:' + res['err']);
            } else {
                makeHtml(res);
            }
            //loading削除
            removeLoading();
        });
    }, 400);

    //エラー
    $.when(def).fail(function (XMLHttpRequest, textStatus, errorThrown) {
        console.log("XMLHttpRequest : " + XMLHttpRequest.status);
        console.log("textStatus : " + textStatus);
        console.log("errorThrown : " + errorThrown.message);
        alert('通信に失敗しました。');
        removeLoading();
    });

    //完了（成否問わず）
    $.when(def).always(function () {
        //submit無効解除
        frmObj.removeAttr('onsubmit');
    });
}
function makeHtml(res) {
    //高さを維持
    $('#result-top').css('min-height', $('#result-top').outerHeight());//正常

    let resWord = res['word'];
    console.log('resWord:' + resWord);

    //キーワード表示
    $('#keyword').html(resWord);

    //URLリストリセット
    $('ul#res-links').html('');

    //URLリスト追加
    $.each(res['query'], function (siteName, resVal) {
        //debug
        //console.log(siteName);
        $('ul#res-links')
            .append('<li><a href="' + resVal['url'] + '"><img src="' + resVal['favicon'] + '" alt="fav">' + siteName + '</a></li>')
        $.each(resVal, function (valName, val) {
            //debug
             console.log(valName + ':' + val);
        });
    });

    $('#result-area').fadeIn();
}

//loading表示
function dispLoading() {
    $('#loading').removeClass("is-hide");
}

// Loadingイメージ削除関数
function removeLoading() {
    $('#loading').addClass("is-hide");
}