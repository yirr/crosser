// JavaScript Document
$(document).ready(
    function () {
        //Amazon Suggestの表示
        $('#search-box').autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: "http://completion.amazon.co.jp/search/complete",
                    data: {mkt: '6', method: 'completion', 'search-alias': 'aps', q: request.term},
                    dataType: "jsonp",
                    type: "GET",
                    success: function (data) {
                        response(data[1]);
                    }
                });
            },
            delay: 200
        });
    }
);

//Open All Pagesボタンイベント
$("#open-all-btn").click(function(){
   $.each($("ul#res-links li"), function(){
      let href = $(this).find("a").attr("href");
      window.open(href);
   });
});

//Open All Pagesボタンイベント
$("#btn-box").click(function(){
    $('form[name=search-frm]').submit();
});

//howToクリック時(チュートリアル開始)
$('a#how-to').click(function(event){
    event.preventDefault();
    tutorial();
});
//Contactクリック時
$('a#contact').click(function(event){
    event.preventDefault();
    //html.clickで表示が無効化されるのを防止
    event.stopPropagation();
    $('#contact-tooltip').fadeToggle();
});

//周囲クリックでtooltipを消す
$('html').click(function() {
    $('#contact-tooltip').fadeOut();
});

function tutorial() {
    $('#result-area').fadeIn().addClass('disable');

    introJs().setOptions({
        'doneLabel': '完了!',
        'prevLabel': '戻る',
        'nextLabel': '次へ',
        'hidePrev': true,
    }).start()
      .onexit(function() {
          $('#result-area').fadeOut().removeClass('disable');
    });
}